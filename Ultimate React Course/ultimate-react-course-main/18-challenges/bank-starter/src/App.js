import "./styles.css";
import { useReducer, useState } from "react";

const initialState = {
  balance: 0,
  loan: 0,
  isActive: false,
};

const DEPOSIT_AMOUNT = 150;
const WITHDRAW_AMOUNT = 50;
const LOAN_AMOUNT = 5000;

function reducer(state, action, payload) {
  const isLoanGreaterThanBalance = state.loan > state.balance;
  const canWithdraw = state.balance > 0;

  switch (action.type) {
    case "openAccount":
      return { ...state, balance: state.balance + 500, isActive: true };
    case "deposit":
      return { ...state, balance: state.balance + action.payload };
    case "withdraw":
      return {
        ...state,
        balance: canWithdraw ? state.balance - action.payload : state.balance,
      };
    case "loan":
      return {
        ...state,
        balance: state.balance + action.payload,
        loan: action.payload,
      };
    case "payLoan":
      return {
        ...state,
        balance: isLoanGreaterThanBalance ? 0 : state.balance - LOAN_AMOUNT,
        loan: isLoanGreaterThanBalance ? LOAN_AMOUNT - state.balance : 0,
      };
    case "closeAccount":
      return { ...state, isActive: false };

    default:
      throw new Error("Action unknown");
  }
}

export default function App() {
  const [{ balance, loan, isActive }, dispatch] = useReducer(
    reducer,
    initialState
  );

  const [depositAmount, setDepositAmount] = useState(DEPOSIT_AMOUNT);
  const [withdrawAmount, setWithdrawAmount] = useState(WITHDRAW_AMOUNT);
  const [loanAmount, setLoanAmount] = useState(LOAN_AMOUNT);

  const isBalanceAndLoanZero = balance === 0 && loan === 0;

  return (
    <div className="App">
      <h1>useReducer Bank Account</h1>
      <p>Balance: {balance}</p>
      <p>Loan: {loan}</p>

      <p>
        <button
          onClick={() => {
            dispatch({ type: "openAccount" });
          }}
          disabled={isActive}
        >
          Open account
        </button>
      </p>
      <p>
        <button
          onClick={() => {
            dispatch({ type: "deposit", payload: depositAmount });
          }}
          disabled={!isActive}
        >
          Deposit {depositAmount}
        </button>
        <input
          type="text"
          value={depositAmount}
          onChange={(e) => setDepositAmount(+e.target.value)}
          placeholder="Enter deposit amount"
          disabled={!isActive}
        />
      </p>
      <p>
        <button
          onClick={() => {
            dispatch({ type: "withdraw", payload: withdrawAmount });
          }}
          disabled={!isActive}
        >
          Withdraw 50
        </button>
        <input
          type="text"
          value={withdrawAmount}
          onChange={(e) => setWithdrawAmount(+e.target.value)}
          placeholder="Enter withdraw amount"
          disabled={!isActive}
        />
      </p>
      <p>
        <button
          onClick={() => {
            dispatch({ type: "loan", payload: loanAmount });
          }}
          disabled={loan > 0 ? isActive : !isActive}
        >
          Request a loan of 5000
        </button>
        <input
          type="text"
          value={loanAmount}
          onChange={(e) => setLoanAmount(+e.target.value)}
          placeholder="Enter loan amount"
          disabled={!isActive}
        />
      </p>
      <p>
        <button
          onClick={() => {
            dispatch({ type: "payLoan" });
          }}
          disabled={!isActive}
        >
          Pay loan
        </button>
      </p>
      <p>
        <button
          onClick={() => {
            dispatch({ type: "closeAccount" });
          }}
          disabled={isBalanceAndLoanZero ? !isActive : isActive}
        >
          Close account
        </button>
      </p>
    </div>
  );
}
