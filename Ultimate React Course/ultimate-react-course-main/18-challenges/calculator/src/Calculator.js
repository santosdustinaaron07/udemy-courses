import BillInput from "./BillInput";
import SelectPercentage from "./SelectPercentage";
import Output from "./Output";
import Reset from "./Reset";

import { useState } from "react";

export default function Calculator() {
  const [bill, setBill] = useState("");
  const [percentage1, setPercentage1] = useState(0);
  const [percentage2, setPercentage2] = useState(0);

  const tip = bill * ((percentage1 + percentage2) / 2 / 100);

  function handleReset() {
    setBill("");
    setPercentage1(0);
    setPercentage2(0);
  }

  return (
    <div>
      <div>
        <BillInput bill={bill} setBill={setBill} />
        <SelectPercentage percentage={percentage1} onChange={setPercentage1}>
          How did you like the service?
        </SelectPercentage>
        <SelectPercentage percentage={percentage2} onChange={setPercentage2}>
          How did your friend like the service?
        </SelectPercentage>
        {bill > 0 ? (
          <>
            <Output bill={bill} totalTip={tip} />
            <Reset onClick={handleReset} />
          </>
        ) : (
          ""
        )}
      </div>
    </div>
  );
}
