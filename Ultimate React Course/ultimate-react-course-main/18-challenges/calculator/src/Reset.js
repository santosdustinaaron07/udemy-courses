export default function Reset({ onClick }) {
  return (
    <div>
      <button onClick={onClick}>Reset</button>
    </div>
  );
}
