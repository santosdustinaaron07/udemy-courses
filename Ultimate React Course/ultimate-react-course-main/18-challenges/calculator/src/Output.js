export default function Output({ bill, totalTip }) {
  const total = bill + totalTip;

  return (
    <div>
      <h3>
        You pay Php{total} (Php{bill} + Php{totalTip} tip)
      </h3>
    </div>
  );
}
