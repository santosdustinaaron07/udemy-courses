import { useState } from "react";

import "./styles.css";

export default function App() {
  return (
    <div className="App">
      <Counter />
    </div>
  );
}

function Counter() {
  const [step, setStep] = useState(1);
  const [count, setCount] = useState(0);

  const date = new Date("june 21 2027");
  date.setDate(date.getDate() + count);

  function handleReset() {
    setStep(1);
    setCount(0);
  }

  return (
    <div>
      <Step step={step} setStep={setStep} />
      <Count count={count} setCount={setCount} step={step} />
      {/* <Date count={count} /> */}

      <div>
        <p>
          <span>
            {count === 0
              ? "Today is "
              : count > 0
              ? `${count} days from today is `
              : `${Math.abs(count)} days ago was `}
          </span>
          <span>{date.toDateString()}</span>
        </p>
      </div>

      <div>
        <button onClick={handleReset}>Reset</button>
      </div>
    </div>
  );
}

function Step({ step, setStep }) {
  // function handleDecreaseStep() {
  //   setStep(step - 1);
  // }

  // function handleIncreaseStep() {
  //   setStep(step + 1);
  // }

  return (
    <div>
      {/* <button onClick={handleDecreaseStep}>-</button>
      <span>Step: {step}</span>
      <button onClick={handleIncreaseStep}>+</button> */}

      <input
        type="range"
        min="0"
        max="100"
        value={step}
        onChange={(e) => setStep(Number(e.target.value))}
      />
      {step}
    </div>
  );
}

function Count({ count, setCount, step }) {
  function handleDecreaseCount() {
    setCount(count - step);
  }

  function handleIncreaseCount() {
    setCount(count + step);
  }

  return (
    <div>
      <button onClick={handleDecreaseCount}>-</button>
      {/* <span>Count: {count}</span> */}
      <input
        type="text"
        value={count}
        onChange={(e) => setCount(Number(e.target.value))}
      />
      <button onClick={handleIncreaseCount}>+</button>
    </div>
  );
}

// function Date({ count }) {
//   const date = new Date("june 21 2027");
//   date.setDate(date.getDate() + count);

//   return (
//     <div>
//       <p>
//         <span>
//           {count === 0
//             ? "Today is"
//             : count > 0
//             ? `${count} days from today is`
//             : `${Math.abs(count)} days ago was`}
//         </span>
//         <span>{date.toDateString()}</span>
//       </p>
//     </div>
//   );
// }
