// `https://api.frankfurter.app/latest?amount=100&from=EUR&to=USD`
import { useEffect, useState } from "react";

export default function App() {
  const [amount, setAmount] = useState(0);
  const [curFrom, setCurFrom] = useState("EUR");
  const [curTo, setCurTo] = useState("USD");
  const [isLoading, setIsLoading] = useState(false);
  const [converted, setConverted] = useState(null);

  useEffect(
    function () {
      async function converter() {
        setIsLoading(true);
        const res = await fetch(
          `https://api.frankfurter.app/latest?amount=${amount}&from=${curFrom}&to=${curTo}`
        );
        const data = await res.json();
        setConverted(data.rates[curTo]);
        setIsLoading(false);
      }

      if (curFrom === curTo) return setConverted(amount);
      converter();
    },
    [amount, curFrom, curTo]
  );

  return (
    <div>
      <input
        type="text"
        value={amount}
        onChange={(e) => setAmount(+e.target.value)}
        // disabled={isLoading}
      />
      <select
        value={curFrom}
        onChange={(e) => setCurFrom(e.target.value)}
        disabled={isLoading}
      >
        <option value="USD">USD</option>
        <option value="EUR">EUR</option>
        <option value="CAD">CAD</option>
        <option value="PHP">PHP</option>
      </select>
      <select
        value={curTo}
        onChange={(e) => setCurTo(e.target.value)}
        disabled={isLoading}
      >
        <option value="USD">USD</option>
        <option value="EUR">EUR</option>
        <option value="CAD">CAD</option>
        <option value="PHP">PHP</option>
      </select>
      <p>{converted}</p>
      <p>Amount: {amount}</p>
      <p>Convert From: {curFrom}</p>
      <p>Convert To: {curTo}</p>
    </div>
  );
}
